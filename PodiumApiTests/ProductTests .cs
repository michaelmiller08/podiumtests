using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;

namespace PodiumApiTests
{
    public class ProductTests
    {
        const string ProductsApi = "https://podium-slns-interview.getsandbox.com/products";

        [Test]
        public async Task CheckAllProductsHaveFields()
        {
            var client = new RestClient(ProductsApi);

            var request = new RestRequest(Method.GET);
            var response = await client.ExecuteAsync(request);
            var jObject = JObject.Parse(response.Content);
            var products = (JArray) jObject["data"];

            foreach (var product in products)
            {
                Assert.IsTrue(!string.IsNullOrEmpty(product["id"].ToString()));
                Assert.IsTrue(!string.IsNullOrEmpty(product["name"].ToString()));
                Assert.IsTrue(!string.IsNullOrEmpty(product["lender"].ToString()));
                Assert.IsTrue(!string.IsNullOrEmpty(product["name"].ToString()));
                Assert.IsTrue(!string.IsNullOrEmpty(product["monthlyRepayment"].ToString()));
                Assert.IsTrue(!string.IsNullOrEmpty(product["interestRate"].ToString()));
                Assert.IsTrue(!string.IsNullOrEmpty(product["minAge"].ToString()));
                Assert.IsTrue(!string.IsNullOrEmpty(product["maxAge"].ToString()));
                Assert.IsTrue(!string.IsNullOrEmpty(product["loanToValue"].ToString()));
                Assert.IsTrue(!string.IsNullOrEmpty(product["type"].ToString()));
                Assert.IsTrue(!string.IsNullOrEmpty(product["initialTerm"].ToString()));
            }
        }


    }
}