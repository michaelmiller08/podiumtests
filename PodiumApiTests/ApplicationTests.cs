using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;
using RestSharp.Serialization.Json;

namespace PodiumApiTests
{
    public class ApplicationTests
    {
        const string ProductsApi = "https://podium-slns-interview.getsandbox.com/products";
        const string DefaultName = "John";
        const int DefaultAge = 20;
        const decimal DefaultMortgageAmount = 4000;
        const decimal DefaultPropertyValue = 5000;

        [Test]
        public async Task SuccessfulBody()
        {
            var client = new RestClient(ApplicationsApiUri());
            var application = new Application
            {
                name = DefaultName,
                age = DefaultAge,
                mortgageAmount = DefaultMortgageAmount,
                propertyValue = DefaultPropertyValue
            };

            var json = JsonConvert.SerializeObject(application);
            var request = new RestRequest(Method.POST);
            request.AddParameter("application/json", json, ParameterType.RequestBody);
            var response = await client.ExecuteAsync(request);

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [Test]
        public async Task AllProductApplicationsAreSuccesful()
        {
            var applicationsClient = new RestClient();
            var application = new Application
            {
                name = DefaultName,
                age = DefaultAge,
                mortgageAmount = DefaultMortgageAmount,
                propertyValue = DefaultPropertyValue
            };
            var json = JsonConvert.SerializeObject(application);
            var applicationsRequest = new RestRequest(Method.POST);
            applicationsRequest.AddParameter("application/json", json, ParameterType.RequestBody);

            foreach (var product in await GetProducts())
            {
                var productId = (int)product["id"];
                applicationsClient.BaseUrl = new Uri(ApplicationsApiUri(productId));
                var applicationsResponse = await applicationsClient.ExecuteAsync(applicationsRequest);
                Assert.That(applicationsResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            }
        }

        [Test]
        public async Task AllApplicationsAreRejected()
        {
            var applicationsClient = new RestClient();
            var application = new Application
            {
                name = DefaultName,
                age = 0,
                mortgageAmount = DefaultMortgageAmount,
                propertyValue = DefaultPropertyValue
            };
            var json = JsonConvert.SerializeObject(application);
            var applicationsRequest = new RestRequest(Method.POST);
            applicationsRequest.AddParameter("application/json", json, ParameterType.RequestBody);

            foreach (var product in await GetProducts())
            {
                var productId = (int)product["id"];
                applicationsClient.BaseUrl = new Uri(ApplicationsApiUri(productId));
                var applicationsResponse = await applicationsClient.ExecuteAsync(applicationsRequest);

                var jObject = JObject.Parse(applicationsResponse.Content);
                var applicationsDecision = jObject["decision"];

                Assert.IsTrue(applicationsDecision.ToString() == "rejected");
            }
        }

        [Test]
        public async Task AllApplicationsAreAccepted()
        {
            var applicationsClient = new RestClient();
            var application = new Application
            {
                name = DefaultName,
                age = DefaultAge,
                mortgageAmount = DefaultMortgageAmount,
                propertyValue = DefaultPropertyValue
            };
            var json = JsonConvert.SerializeObject(application);
            var applicationsRequest = new RestRequest(Method.POST);
            applicationsRequest.AddParameter("application/json", json, ParameterType.RequestBody);

            foreach (var product in await GetProducts())
            {
                var productId = (int)product["id"];
                applicationsClient.BaseUrl = new Uri(ApplicationsApiUri(productId));
                var applicationsResponse = await applicationsClient.ExecuteAsync(applicationsRequest);

                var jObject = JObject.Parse(applicationsResponse.Content);
                var applicationsDecision = jObject["decision"];

                Assert.IsTrue(applicationsDecision.ToString() == "accepted");
            }
        }

        [Test]
        public async Task InvalidMinAge()
        {
            var client = new RestClient(ApplicationsApiUri());
            var application = new Application
            {
                name = DefaultName,
                age = 12,
                mortgageAmount = DefaultMortgageAmount,
                propertyValue = DefaultPropertyValue
            };

            var json = JsonConvert.SerializeObject(application);
            var request = new RestRequest(Method.POST);
            request.AddParameter("application/json", json, ParameterType.RequestBody);
            var response = await client.ExecuteAsync(request);

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [Test]
        public async Task InvalidMaxAge()
        {
            var client = new RestClient(ApplicationsApiUri());
            var application = new Application
            {
                name = DefaultName,
                age = 400,
                mortgageAmount = DefaultMortgageAmount,
                propertyValue = DefaultPropertyValue
            };

            var json = JsonConvert.SerializeObject(application);
            var request = new RestRequest(Method.POST);
            request.AddParameter("application/json", json, ParameterType.RequestBody);
            var response = await client.ExecuteAsync(request);

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [Test]
        public async Task InvalidName()
        {
            var client = new RestClient(ApplicationsApiUri());
            var application = new Application
            {
                name = "123456�$%",
                age = DefaultAge,
                mortgageAmount = DefaultMortgageAmount,
                propertyValue = DefaultPropertyValue
            };

            var json = JsonConvert.SerializeObject(application);
            var request = new RestRequest(Method.POST);
            request.AddParameter("application/json", json, ParameterType.RequestBody);
            var response = await client.ExecuteAsync(request);

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [Test]
        public async Task InvalidMortgageAmount()
        {
            var client = new RestClient(ApplicationsApiUri());
            var application = new Application
            {
                name = DefaultName,
                age = DefaultAge,
                mortgageAmount = 0,
                propertyValue = DefaultPropertyValue
            };

            var json = JsonConvert.SerializeObject(application);
            var request = new RestRequest(Method.POST);
            request.AddParameter("application/json", json, ParameterType.RequestBody);
            var response = await client.ExecuteAsync(request);

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [Test]
        public async Task InvalidPropertyValue()
        {
            var client = new RestClient(ApplicationsApiUri());
            var application = new Application
            {
                name = DefaultName,
                age = DefaultAge,
                mortgageAmount = DefaultMortgageAmount,
                propertyValue = 0
            };

            var json = JsonConvert.SerializeObject(application);
            var request = new RestRequest(Method.POST);
            request.AddParameter("application/json", json, ParameterType.RequestBody);
            var response = await client.ExecuteAsync(request);

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [Test]
        public async Task InvalidProductId()
        {
            var client = new RestClient(ApplicationsApiUri(9999999));
            var application = new Application
            {
                name = DefaultName,
                age = DefaultAge,
                mortgageAmount = DefaultMortgageAmount,
                propertyValue = DefaultPropertyValue
            };

            var json = JsonConvert.SerializeObject(application);
            var request = new RestRequest(Method.POST);
            request.AddParameter("application/json", json, ParameterType.RequestBody);
            var response = await client.ExecuteAsync(request);

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        string ApplicationsApiUri(int product = 1)
        {
            return $"https://podium-slns-interview.getsandbox.com/products/{product}/applications";
        }

        async Task<JArray> GetProducts()
        {
            var client = new RestClient(ProductsApi);

            var productsRequest = new RestRequest(Method.GET);
            var productsResponse = await client.ExecuteAsync(productsRequest);
            var jObject = JObject.Parse(productsResponse.Content);
            //Data is list of products
            return (JArray)jObject["data"];
        }
    }
}