﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PodiumApiTests
{
    class Application
    {
        public string name { get; set; }

        public int age { get; set; }

        public decimal mortgageAmount { get; set; }

        public decimal propertyValue { get; set; }
    }
}
