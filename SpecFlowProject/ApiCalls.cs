﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace SpecFlowProject
{
    class ApiCalls
    {
        const string ProductsApi = "https://podium-slns-interview.getsandbox.com/products";

        public async Task<JArray> GetProducts()
        {
            var client = new RestClient(ProductsApi);

            var productsRequest = new RestRequest(Method.GET);
            var productsResponse = await client.ExecuteAsync(productsRequest);
            var jObject = JObject.Parse(productsResponse.Content);
            //Data is list of products
            return (JArray)jObject["data"];
        }
    }
}
