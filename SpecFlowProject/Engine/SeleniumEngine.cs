﻿using System;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;

namespace SpecFlowProject.Engine
{
    public class SeleniumEngine : ISeleniumEngine
    {
        const string ChromeDownloadDirectoryUserPreference = "download.default_directory";
        const string ChromePopUpUserPreference = "disable-popup-blocking";
        const string ChromeDownloadPromptUserPreference = "download.prompt_for_download";
        const string FirefoxDownloadDirectoryPreference = "browser.download.dir";
        const string FirefoxNeverSaveToDiskPopUpPreference = "browser.helperApps.neverAsk.saveToDisk";
        const string DownloadsDirectory = "";
        readonly string _driversDirectory = System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("SpecFlowProject\\bin\\Debug\\net48\\SpecFlowProject.dll", "");
        RemoteWebDriver _driver;


        public SeleniumEngine(SeleniumEngineConfig config)
        {
            SelectBrowser(config.Browser);
            _driver.Manage().Window.Maximize();
            _driver.Navigate().GoToUrl(config.Url);
        }

        public IWebAutomator CreateAutomator()
        {
            return new WebAutomator(_driver);
        }

        void SelectBrowser(BrowserType browserType)
        {
            switch (browserType)
            {
                case BrowserType.Chrome:
                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference(ChromeDownloadDirectoryUserPreference, DownloadsDirectory);
                    chromeOptions.AddUserProfilePreference(ChromePopUpUserPreference, true);
                    chromeOptions.AddUserProfilePreference(ChromeDownloadPromptUserPreference, false);
                    _driver = new ChromeDriver(_driversDirectory, chromeOptions);
                    break;
                case BrowserType.Firefox:
                    var firefoxOptions = new FirefoxOptions();
                    firefoxOptions.SetPreference(FirefoxDownloadDirectoryPreference, DownloadsDirectory);
                    firefoxOptions.SetPreference(FirefoxNeverSaveToDiskPopUpPreference,
                        "application/xml;application/zip");
                    _driver = new FirefoxDriver(firefoxOptions);
                    break;
                case BrowserType.Edge:
                    //There is no support for newer versions of edge at the moment 
                    _driver = new EdgeDriver(_driversDirectory);
                    break;
            }

            SeleniumInitalizer.GetObjectContainer().RegisterInstanceAs<IWebDriver>(_driver);
        }
    }
}