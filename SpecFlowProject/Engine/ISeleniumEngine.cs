﻿namespace SpecFlowProject.Engine
{
    public interface ISeleniumEngine
    {
        IWebAutomator CreateAutomator();
    }


}