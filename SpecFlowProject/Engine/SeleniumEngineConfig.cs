﻿namespace SpecFlowProject.Engine
{
    public class SeleniumEngineConfig
    {
        public BrowserType Browser { get; set; }

        public string Url { get; set; }
    }
}