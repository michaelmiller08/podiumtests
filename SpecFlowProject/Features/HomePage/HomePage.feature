﻿Feature: HomePage

@Smoke Test
Scenario: Homepage page exists
	When I am on the homepage
	Then I expect to see the homepage

@Smoke Test
Scenario: All products have continue button
	When I am on the homepage
	Then I expect to see a continue button for every product

@Smoke Test
Scenario: Continue to products page
	Given I am on the homepage
	When I select a product 
	Then I expect to see the product page

#@Smoke Test
#Scenario: All product title and descriptions are correct
#	When I am on the homepage
#		Then I expect to see the correct product titles and descriptions
	
