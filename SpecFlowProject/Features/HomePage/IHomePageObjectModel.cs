﻿namespace SpecFlowProject.Features.HomePage
{
    public interface IHomePageObjectModel : IBasePageObjectModel
    {
        void CheckAllContinueButtonsExist();

        void ChooseContinue();
    }
}