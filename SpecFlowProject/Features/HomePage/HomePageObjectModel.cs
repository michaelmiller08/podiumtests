﻿using System;
using OpenQA.Selenium;
using SpecFlowProject.Engine;

namespace SpecFlowProject.Features.HomePage
{
    class HomePageObjectModel : BasePageObjectModel, IHomePageObjectModel
    {
        const string TitleMessageId = "message";

        const string TitleMessage =
            "Some awesome mortgage website\r\nHello, we found these mortgages for you:\r\nPlease select a mortgage product to apply.";

        const string ButtonCss = "div#app > div > div > div:nth-of-type(2) > div:nth-of-type(4) > div";
        const int AmountOfButtons = 12;
        readonly IWebAutomator _automator;

        public HomePageObjectModel()
        {
            var engine = SeleniumInitalizer.GetObjectContainer().Resolve<ISeleniumEngine>();
            _automator = engine.CreateAutomator();
        }

        public override void WaitForVisibilityOfControls(TimeSpan timespan)
        {
            _automator.WaitForTextInElement(TimeSpan.FromSeconds(10), By.ClassName(TitleMessageId), TitleMessage);
        }

        public void CheckAllContinueButtonsExist()
        {
            //Assert is not needed here since the "FindElements" will fail test anyway 
            _automator.CheckNumberOfTheSameElement(TimeSpan.FromSeconds(10), By.CssSelector(ButtonCss),
                AmountOfButtons);
        }

        //Params for which one will be here 
        public void ChooseContinue()
        {
            //Assert is not needed here since the "FindElements" will fail test anyway 
            _automator.Click(By.CssSelector(ButtonCss));
        }
    }
}