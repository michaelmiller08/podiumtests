﻿using System;
using System.Threading.Tasks;
using BoDi;
using SpecFlowProject.Features.ProductPage;
using TechTalk.SpecFlow;

namespace SpecFlowProject.Features.HomePage
{
    [Binding]
    public class HomePageSteps
    {
        readonly IHomePageObjectModel _homePageObjectModel;
        readonly IProductPageObjectModel _productPageObjectModel;

        //ToDo: Create pageobjectmodel factory to use in SeleniumInitalizer instead of registering here
        public HomePageSteps(IObjectContainer container)
        {
            try
            {
                _homePageObjectModel = container.Resolve<IHomePageObjectModel>();
            }
            catch (ObjectContainerException)
            {
                container.RegisterTypeAs<HomePageObjectModel, IHomePageObjectModel>();
                _homePageObjectModel = container.Resolve<IHomePageObjectModel>();
            }

            try
            {
                _productPageObjectModel = container.Resolve<IProductPageObjectModel>();
            }
            catch (ObjectContainerException)
            {
                container.RegisterTypeAs<ProductPageObjectModel, IProductPageObjectModel>();
                _productPageObjectModel = container.Resolve<IProductPageObjectModel>();
            }
        }

        //Page <Interface>  GetPageObjectModel<Page, Interface>(IObjectContainer container)
        //{
        //    try
        //    {
        //        return container.Resolve<Page>();
        //    }
        //    catch (ObjectContainerException)
        //    {
        //        container.RegisterTypeAs<TPageObjectModel, TIPageObjectModel>();
        //        return container.Resolve<TIPageObjectModel>();
        //    }
        //}

        [Given(@"I am on the homepage")]
        public void GivenIAmOnTheHomepage()
        {
            //Nothing to do here since site directs you 
        }

        [When(@"I am on the homepage")]
        public void WhenIAmOnTheHomepage()
        {
            //Nothing to do here since site directs you 
        }

        [When(@"I select a product")]
        public void WhenISelectAProduct()
        {
            _homePageObjectModel.ChooseContinue();
        }

        [Then(@"I expect to see the homepage")]
        public void ThenIExpectToSeeTheHomepage()
        {
            _homePageObjectModel.WaitForVisibilityOfControls(TimeSpan.FromSeconds(10));
        }

        [Then(@"I expect to see the product page")]
        public void ThenIExpectToSeeTheProductPage()
        {
            _productPageObjectModel.WaitForVisibilityOfControls(TimeSpan.FromSeconds(10));
        }


        [Then(@"I expect to see a continue button for every product")]
        public void ThenIExpectToSeeAContinueButtonForEveryProduct()
        {
            _homePageObjectModel.CheckAllContinueButtonsExist();
        }

        //[Then(@"I expect to see the correct product titles and descriptions")]
        //public async Task ThenIExpectToSeeTheCorrectProductTitlesAndDescriptions()
        //{
        //    foreach (var product in await new ApiCalls().GetProducts())
        //    {
        //        var productName = product["name"];
        //        var description = $"Mortgage provided by {product["lender"]["name"]}";
        //        var productLender = product["lender"]["name"];
        //        var monthlyRepayment = product["monthlyRepayment"];
        //        var interestRate = product["interestRate"];
        //        var type = product["type"];

        //        _homePageObjectModel.CheckProductTitlesAndDescriptions(productName.ToString());
        //    }
        //}

    }
}