﻿using System;
using SpecFlowProject.Engine;
using TechTalk.SpecFlow;

namespace SpecFlowProject.Features.ProductPage
{
    class ProductPageObjectModel : BasePageObjectModel, IProductPageObjectModel
    {
        IWebAutomator _automator;

        public ProductPageObjectModel()
        {
            var engine = SeleniumInitalizer.GetObjectContainer().Resolve<ISeleniumEngine>();
            _automator = engine.CreateAutomator();
        }

        public override void WaitForVisibilityOfControls(TimeSpan timespan)
        {
            ScenarioContext.Current.Pending();
        }
    }
}