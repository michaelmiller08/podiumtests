﻿using System;
using BoDi;
using OpenQA.Selenium;
using SpecFlowProject.Engine;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Bindings;

namespace SpecFlowProject
{
    public static class SeleniumInitalizer
    {
        public static void Initialize(SeleniumEngineConfig config)
        {
            if (!IsInitialized())
            {
                var container = GetObjectContainer();
                container.RegisterTypeAs<SeleniumEngine, ISeleniumEngine>();
                container.RegisterTypeAs<WebAutomator, IWebAutomator>();
                container.RegisterInstanceAs(config);
            }
        }

        public static IObjectContainer GetObjectContainer()
        {
            var context = ScenarioContext.Current;

            if (context == null) throw new Exception("Cannot get object container, scenario context is null");

            var container = context.GetBindingInstance(typeof(IObjectContainer)) as IObjectContainer;

            if (container == null) throw new Exception("Provided object container is null");

            return container;
        }

        public static void CleanUp()
        {
            GetObjectContainer().Resolve<IWebDriver>().Quit();

            //if (Directory.Exists(GenericStrings.DownloadsDirectory))
            //{
            //    Directory.Delete(GenericStrings.DownloadsDirectory, true);
            //}
        }

        static bool IsInitialized()
        {
            bool isInitialized;

            try
            {
                var container = GetObjectContainer();
                isInitialized = container.Resolve<ISeleniumEngine>() != null;
            }
            catch (ObjectContainerException)
            {
                isInitialized = false;
            }

            return isInitialized;
        }
    }
}