﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpecFlowProject
{
    public enum BrowserType
    {
        Chrome,
        Firefox,
        Edge
    }
}
