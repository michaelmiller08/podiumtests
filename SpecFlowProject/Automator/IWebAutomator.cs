﻿using System;
using System.IO;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using WaitHelpers = SeleniumExtras.WaitHelpers;

namespace SpecFlowProject
{
    public interface IWebAutomator
    {
        void Click(By element);

        void WaitForElement(TimeSpan timeout, params By[] elements);

        void WriteToInputField(By elementToFind, string value);

        void WaitForNoElement(TimeSpan timeout, params By[] elements);

        string RetrieveTextFromElement(TimeSpan timeout, By element);

        void WaitUntilFileDownloaded(TimeSpan timeout, string filePath);

        void WaitForTextInElement(TimeSpan timeout, By element, string text);

        void WaitForNoTextInElement(TimeSpan timeout, By element, string text);

        void WaitForPartialTextInElement(TimeSpan timeout, By element, string text);

        void CheckNumberOfTheSameElement(TimeSpan timeSpan, By element, int expectedCount);
    }
}