﻿using System;
using System.IO;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace SpecFlowProject
{
    public class WebAutomator : IWebAutomator
    {
        readonly RemoteWebDriver _webDriver;

        public WebAutomator(RemoteWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        public void Click(By element)
        {
            var wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(10));
            wait.Message = $"{element} could not be clicked";
            wait.Until(drv => drv.FindElement(element)).Click();
        }

        public void WaitForElement(TimeSpan timeout, params By[] elements)
        {
            var wait = new WebDriverWait(_webDriver, timeout);
            foreach (var element in elements)
            {
                wait.Message = $"{element} was not found";
                wait.Until(drv => drv.FindElement(element));
            }
        }

        public void WriteToInputField(By elementToFind, string value)
        {
            var wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(10));
            var element = wait.Until(drv => drv.FindElement(elementToFind));
            element.Clear();
            element.SendKeys(value);
        }

        public void WaitForNoElement(TimeSpan timeout, params By[] elements)
        {
            var wait = new WebDriverWait(_webDriver, timeout);
            foreach (var element in elements)
            {
                wait.Message = $"{element} was found";
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(element));
            }
        }

        public string RetrieveTextFromElement(TimeSpan timeout, By element)
        {
            var wait = new WebDriverWait(_webDriver, timeout);
            wait.Message = $"Could not retrieve text from {element}";
            return wait.Until(drv => drv.FindElement(element).Text);
        }

        public void WaitUntilFileDownloaded(TimeSpan timeout, string filePath)
        {
            var wait = new WebDriverWait(_webDriver, timeout);
            wait.Message = $"Could not find file {filePath}";
            wait.Until(drv => File.Exists(filePath));
        }

        public void WaitForTextInElement(TimeSpan timeout, By element, string text)
        {
            var wait = new WebDriverWait(_webDriver, timeout);
            wait.Message = $"Could not find a {element} that contains {text}";
            wait.Until(drv => drv.FindElement(element).Text == text);
        }

        public void WaitForNoTextInElement(TimeSpan timeout, By element, string text)
        {
            var wait = new WebDriverWait(_webDriver, timeout);
            wait.Message = $"Found a {element} that contains {text}";
            wait.Until(ExpectedConditions.InvisibilityOfElementWithText(element, text));
        }

        public void WaitForPartialTextInElement(TimeSpan timeout, By element, string text)
        {
            var wait = new WebDriverWait(_webDriver, timeout);
            wait.Message = $"Could not find a {element} that contains {text}";
            wait.Until(drv => _webDriver.FindElements(element).Any(e => e.Text.Contains(text)));
        }

        public void CheckNumberOfTheSameElement(TimeSpan timeSpan, By element, int expectedCount)
        {
            var wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(10));
            wait.Message = $"Could not find {expectedCount} of {element}";
            wait.Until(drv => drv.FindElements(element).Count == expectedCount);
        }
    }
}