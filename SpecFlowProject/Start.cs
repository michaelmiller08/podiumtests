﻿using SpecFlowProject.Engine;
using TechTalk.SpecFlow;

namespace SpecFlowProject
{
    [Binding]
    class Start
    {
        [BeforeScenario]
        public void Setup()
        {
            var config = new SeleniumEngineConfig
            {
                //Best practice is to create a pipleline which replaces this file with different browser configs
                Browser = BrowserType.Chrome,
                Url = "https://podium-test-65804.firebaseapp.com/"
            };
            SeleniumInitalizer.Initialize(config);
        }

        [AfterScenario]
        public void CleanUp()
        {
            SeleniumInitalizer.CleanUp();
        }
    }
}